import 'dart:ui';

import 'package:epitech_pokemons/pokemon.dart';

final pokemons = [
  Pokemon(
    id: 1,
    name: "Bulbizarre",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Bulbizarre passe son temps à faire la sieste sous le soleil.\nIl y a une graine sur son dos. Il absorbe les rayons du soleil\npour faire doucement pousser la graine.",
    height: "0,7 m",
    weight: "6,9 kg",
    category: "Graine",
    abilities: "Engrais",
    color: Color(0xFF779f87),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/001.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png",
    evolutionChain: [1, 2, 3],
  ),
  Pokemon(
    id: 2,
    name: "Herbizarre",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Un bourgeon a poussé sur le dos de ce Pokémon. Pour en\nsupporter le poids, Herbizarre a dû se muscler les pattes.\nLorsqu'il commence à se prélasser au soleil, ça signifie que\nson bourgeon va éclore, donnant naissance à une fleur.",
    height: "1,0 m",
    weight: "13,0 kg",
    category: "Graine",
    abilities: "Engrais",
    color: Color(0xFF6c8b8a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/002.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/002.png",
    evolutionChain: [1, 2, 3],
  ),
  Pokemon(
    id: 3,
    name: "Florizarre",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Une belle fleur se trouve sur le dos de Florizarre.\nElle prend une couleur vive lorsqu'elle est bien nourrie et bien\nensoleillée. Le parfum de cette fleur peut apaiser les gens.",
    height: "2,4 m",
    weight: "155,5 kg",
    category: "Graine",
    abilities: "Isograisse",
    color: Color(0xFF707a74),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/003.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/003.png",
    evolutionChain: [1, 2, 3],
  ),
  Pokemon(
    id: 4,
    name: "Salamèche",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "La flamme qui brûle au bout de sa queue indique l'humeur\nde ce Pokémon. Elle vacille lorsque Salamèche est content.\nEn revanche, lorsqu'il s'énerve, la flamme prend\nde l'importance et brûle plus ardemment.",
    height: "0,6 m",
    weight: "8,5 kg",
    category: "Lézard",
    abilities: "Brasier",
    color: Color(0xFFc69d7e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/004.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png",
    evolutionChain: [4, 5, 6],
  ),
  Pokemon(
    id: 5,
    name: "Reptincel",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "Reptincel lacère ses ennemis sans pitié grâce à ses griffes\nacérées. S'il rencontre un ennemi puissant, il devient agressif\net la flamme au bout de sa queue s'embrase et prend\nune couleur bleu clair.",
    height: "1,1 m",
    weight: "19,0 kg",
    category: "Flamme",
    abilities: "Brasier",
    color: Color(0xFFc37d6c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/005.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png",
    evolutionChain: [4, 5, 6],
  ),
  Pokemon(
    id: 6,
    name: "Dracaufeu",
    types: ["fire", "flying"],
    weaknesses: ["rock", "electric", "water"],
    description:
        "Dracaufeu parcourt les cieux pour trouver des adversaires\nà sa mesure. Il crache de puissantes flammes capables de\nfaire fondre n'importe quoi. Mais il ne dirige jamais son souffle\ndestructeur vers un ennemi plus faible.",
    height: "1,7 m",
    weight: "100,5 kg",
    category: "Flamme",
    abilities: "Sécheresse",
    color: Color(0xFF9d8d6e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/006.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png",
    evolutionChain: [4, 5, 6],
  ),
  Pokemon(
    id: 7,
    name: "Carapuce",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "La carapace de Carapuce ne sert pas qu'à le protéger.\nLa forme ronde de sa carapace et ses rainures lui permettent\nd'améliorer son hydrodynamisme.\nCe Pokémon nage extrêmement vite.",
    height: "0,5 m",
    weight: "9,0 kg",
    category: "Minitortue",
    abilities: "Torrent",
    color: Color(0xFF86a4a8),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/007.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png",
    evolutionChain: [7, 8, 9],
  ),
  Pokemon(
    id: 8,
    name: "Carabaffe",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Carabaffe a une large queue recouverte d'une épaisse\nfourrure. Elle devient de plus en plus foncée avec l'âge.\nLes éraflures sur la carapace de ce Pokémon témoignent\nde son expérience au combat.",
    height: "1,0 m",
    weight: "22,5 kg",
    category: "Tortue",
    abilities: "Torrent",
    color: Color(0xFFa6b4c1),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/008.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/008.png",
    evolutionChain: [7, 8, 9],
  ),
  Pokemon(
    id: 9,
    name: "Tortank",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Tortank dispose de canons à eau émergeant de sa carapace.\nIls sont très précis et peuvent envoyer des balles d'eau\ncapables de faire mouche sur une cible située à plus de 50 m.",
    height: "1,6 m",
    weight: "101,1 kg",
    category: "Carapace",
    abilities: "Méga Blaster",
    color: Color(0xFF89868a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/009.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/009.png",
    evolutionChain: [7, 8, 9],
  ),
  Pokemon(
    id: 10,
    name: "Chenipan",
    types: ["bug"],
    weaknesses: ["fire", "flying", "rock"],
    description:
        "Chenipan a un appétit d'ogre. Il peut engloutir des feuilles\nplus grosses que lui. Les antennes de ce Pokémon dégagent\nune odeur particulièrement entêtante.",
    height: "0,3 m",
    weight: "2,9 kg",
    category: "Ver",
    abilities: "Écran Poudre",
    color: Color(0xFF95a36b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/010.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/010.png",
    evolutionChain: [10, 11, 12],
  ),
  Pokemon(
    id: 11,
    name: "Chrysacier",
    types: ["bug"],
    weaknesses: ["fire", "flying", "rock"],
    description:
        "La carapace protégeant ce Pokémon est dure comme du\nmétal. Chrysacier ne bouge pas beaucoup. Il reste immobile\npour préparer les organes à l'intérieur de sa carapace en vue\nd'une évolution future.",
    height: "0,7 m",
    weight: "9,9 kg",
    category: "Cocon",
    abilities: "Mue",
    color: Color(0xFF85a45a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/011.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/011.png",
    evolutionChain: [10, 11, 12],
  ),
  Pokemon(
    id: 12,
    name: "Papilusion",
    types: ["bug", "flying"],
    weaknesses: ["rock", "electric", "fire", "flying", "ice"],
    description:
        "Papilusion est très doué pour repérer le délicieux nectar qu'il\nbutine dans les fleurs. Il peut détecter, extraire et transporter\nle nectar de fleurs situées à plus de 10 km de son nid.",
    height: "1,1 m",
    weight: "32,0 kg",
    category: "Papillon",
    abilities: "Œil Composé",
    color: Color(0xFFabadb6),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/012.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/012.png",
    evolutionChain: [10, 11, 12],
  ),
  Pokemon(
    id: 13,
    name: "Aspicot",
    types: ["bug", "poison"],
    weaknesses: ["fire", "flying", "psychic", "rock"],
    description:
        "L'odorat d'Aspicot est extrêmement développé. Il lui suffit de\nrenifler ses feuilles préférées avec son gros appendice nasal\npour les reconnaître entre mille.",
    height: "0,3 m",
    weight: "3,2 kg",
    category: "Insectopic",
    abilities: "Écran Poudre",
    color: Color(0xFFa27f66),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/013.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/013.png",
    evolutionChain: [13, 14, 15],
  ),
  Pokemon(
    id: 14,
    name: "Coconfort",
    types: ["bug", "poison"],
    weaknesses: ["fire", "flying", "psychic", "rock"],
    description:
        "Coconfort est la plupart du temps immobile et reste\naccroché à un arbre. Cependant, intérieurement, il est\ntrès actif, car il se prépare pour sa prochaine évolution.\nEn touchant sa carapace, on peut sentir sa chaleur.",
    height: "0,6 m",
    weight: "10,0 kg",
    category: "Cocon",
    abilities: "Mue",
    color: Color(0xFFb09a58),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/014.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/014.png",
    evolutionChain: [13, 14, 15],
  ),
  Pokemon(
    id: 15,
    name: "Dardargnan",
    types: ["bug", "poison"],
    weaknesses: ["fire", "flying", "psychic", "rock"],
    description:
        "Dardargnan est extrêmement possessif. Il vaut mieux ne pas\ntoucher son nid si on veut éviter d'avoir des ennuis.\nLorsqu'ils sont en colère, ces Pokémon attaquent en masse.",
    height: "1,4 m",
    weight: "40,5 kg",
    category: "Guêpoison",
    abilities: "Adaptabilité",
    color: Color(0xFFaca698),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/015.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/015.png",
    evolutionChain: [13, 14, 15],
  ),
  Pokemon(
    id: 16,
    name: "Roucool",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Roucool a un excellent sens de l'orientation. Il est capable de\nretrouver son nid sans jamais se tromper, même s'il est très\nloin de chez lui et dans un environnement qu'il ne connaît pas.",
    height: "0,3 m",
    weight: "1,8 kg",
    category: "Minoiseau",
    abilities: "Regard Vif",
    color: Color(0xFFa48d6b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/016.png",
    evolutionChain: [16, 17, 18],
  ),
  Pokemon(
    id: 17,
    name: "Roucoups",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Roucoups utilise une vaste surface pour son territoire.\nCe Pokémon surveille régulièrement son espace aérien.\nSi quelqu'un pénètre sur son territoire, il corrige l'ennemi\nsans pitié d'un coup de ses terribles serres.",
    height: "1,1 m",
    weight: "30,0 kg",
    category: "Oiseau",
    abilities: "Regard Vif",
    color: Color(0xFFb69c7e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/017.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/017.png",
    evolutionChain: [16, 17, 18],
  ),
  Pokemon(
    id: 18,
    name: "Roucarnage",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Ce Pokémon est doté d'un plumage magnifique et luisant.\nBien des Dresseurs sont captivés par la beauté fatale de\nsa huppe et décident de choisir Roucarnage comme leur\nPokémon favori.",
    height: "2,2 m",
    weight: "50,5 kg",
    category: "Oiseau",
    abilities: "Annule Garde",
    color: Color(0xFFb99d7d),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/018.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/018.png",
    evolutionChain: [16, 17, 18],
  ),
  Pokemon(
    id: 19,
    name: "Rattata",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Rattata est extrêmement prudent. Même lorsqu'il est endormi,\nil fait pivoter ses oreilles pour écouter autour de lui.\nEn ce qui concerne son habitat, il n'est vraiment pas difficile.\nIl peut faire son nid n'importe où.",
    height: "0,3 m",
    weight: "3,8 kg",
    category: "Souris",
    abilities: "Agitation",
    color: Color(0xFF9b8596),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/019.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/019.png",
    evolutionChain: [19, 20],
  ),
  Pokemon(
    id: 20,
    name: "Rattatac",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Les crocs robustes de Rattatac poussent constamment.\nPour éviter qu'ils raclent le sol, il se fait les dents sur\ndes cailloux ou des troncs d'arbre. Il lui arrive même\nde ronger les murs des maisons.",
    height: "0,7 m",
    weight: "25,5 kg",
    category: "Souris",
    abilities: "Agitation",
    color: Color(0xFFa68967),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/020.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/020.png",
    evolutionChain: [19, 20],
  ),
  Pokemon(
    id: 21,
    name: "Piafabec",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Piafabec crie tellement fort qu'il peut être entendu à 1 km\nde distance. Ces Pokémon se préviennent d'un danger\nen entonnant une mélopée très aiguë, qu'ils se renvoient\nles uns les autres, comme un écho.",
    height: "0,3 m",
    weight: "2,0 kg",
    category: "Minoiseau",
    abilities: "Regard Vif",
    color: Color(0xFFa7786d),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/021.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/021.png",
    evolutionChain: [21, 22],
  ),
  Pokemon(
    id: 22,
    name: "Rapasdepic",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "On reconnaît un Rapasdepic à son long cou et à son bec\nallongé. Ces attributs lui permettent d'attraper facilement\nses proies dans la terre ou dans l'eau. Il bouge son bec\nlong et fin avec une grande agilité.",
    height: "1,2 m",
    weight: "38,0 kg",
    category: "Bec-Oiseau",
    abilities: "Regard Vif",
    color: Color(0xFFa17a5e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/022.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/022.png",
    evolutionChain: [21, 22],
  ),
  Pokemon(
    id: 23,
    name: "Abo",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Abo s'enroule en spirale pour dormir. Sa tête reste relevée\nde telle sorte que cette position lui permette de réagir\nrapidement si une menace survenait.",
    height: "2,0 m",
    weight: "6,9 kg",
    category: "Serpent",
    abilities: "Mue",
    color: Color(0xFF917081),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/023.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/023.png",
    evolutionChain: [23, 24],
  ),
  Pokemon(
    id: 24,
    name: "Arbok",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Ce Pokémon doté d'une force extraordinaire peut étrangler\nses proies avec son corps. Il peut même écraser des tonneaux\nmétalliques. Une fois sous l'étreinte d'Arbok, il est impossible\nde lui échapper.",
    height: "3,5 m",
    weight: "65,0 kg",
    category: "Cobra",
    abilities: "Mue",
    color: Color(0xFF7c707b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/024.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/024.png",
    evolutionChain: [23, 24],
  ),
  Pokemon(
    id: 25,
    name: "Pikachu",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "Ce Pokémon dispose de petites poches dans les joues pour\nstocker de l'électricité. Elles semblent se charger pendant\nque Pikachu dort. Il libère parfois un peu d'électricité\nlorsqu'il n'est pas encore bien réveillé.",
    height: "0,4 m",
    weight: "6,0 kg",
    category: "Souris",
    abilities: "Statik",
    color: Color(0xFFceb26b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/025.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/025.png",
    evolutionChain: [25, 26],
  ),
  Pokemon(
    id: 26,
    name: "Raichu",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "Ce Pokémon libère un faible champ électrique tout autour\nde son corps, ce qui le rend légèrement lumineux dans le noir.\nRaichu plante sa queue dans le sol pour évacuer de\nl'électricité.",
    height: "0,7 m",
    weight: "21,0 kg",
    category: "Souris",
    abilities: "Surf Caudal",
    color: Color(0xFFbfa26f),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/026.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/026.png",
    evolutionChain: [25, 26],
  ),
  Pokemon(
    id: 27,
    name: "Sabelette",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "Sabelette a une peau très sèche et extrêmement dure.\nCe Pokémon peut s'enrouler sur lui-même pour repousser\nles attaques. La nuit, il s'enterre dans le sable du désert\npour dormir.",
    height: "0,7 m",
    weight: "40,0 kg",
    category: "Souris",
    abilities: "Rideau Neige",
    color: Color(0xFFbaa97e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/027.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/027.png",
    evolutionChain: [27, 28],
  ),
  Pokemon(
    id: 28,
    name: "Sablaireau",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "Sablaireau peut enrouler son corps pour prendre la forme\nd'une balle hérissée de pointes. Ce Pokémon essaie de faire\npeur à son ennemi en le frappant avec ses pointes.\nPuis, il se rue sur lui avec ses griffes acérées.",
    height: "1,2 m",
    weight: "55,0 kg",
    category: "Souris",
    abilities: "Rideau Neige",
    color: Color(0xFF9d835a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/028.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/028.png",
    evolutionChain: [27, 28],
  ),
  Pokemon(
    id: 29,
    name: "Nidoran♀",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Nidoran♀ est couvert de pointes qui sécrètent un poison\npuissant. On pense que ce petit Pokémon a développé\nces pointes pour se défendre. Lorsqu'il est en colère,\nune horrible toxine sort de sa corne.",
    height: "0,4 m",
    weight: "7,0 kg",
    category: "Vénépic",
    abilities: "Point Poison",
    color: Color(0xFFa0afc5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/029.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/029.png",
    evolutionChain: [29, 30, 31],
  ),
  Pokemon(
    id: 30,
    name: "Nidorina",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Lorsqu'un Nidorina est avec ses amis ou sa famille, il replie\nses pointes pour ne pas blesser ses proches. Ce Pokémon\ndevient vite nerveux lorsqu'il est séparé de son groupe.",
    height: "0,8 m",
    weight: "20,0 kg",
    category: "Vénépic",
    abilities: "Point Poison",
    color: Color(0xFF8daab5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/030.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/030.png",
    evolutionChain: [29, 30, 31],
  ),
  Pokemon(
    id: 31,
    name: "Nidoqueen",
    types: ["poison", "ground"],
    weaknesses: ["ground", "ice", "psychic", "water"],
    description:
        "Le corps de Nidoqueen est protégé par des écailles\nextrêmement dures. Il aime envoyer ses ennemis voler en leur\nfonçant dessus. Ce Pokémon utilise toute sa puissance\nlorsqu'il protège ses petits.",
    height: "1,3 m",
    weight: "60,0 kg",
    category: "Perceur",
    abilities: "Point Poison",
    color: Color(0xFF7b8f9b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/031.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/031.png",
    evolutionChain: [29, 30, 31],
  ),
  Pokemon(
    id: 32,
    name: "Nidoran♂",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Nidoran♂ a développé des muscles pour bouger\nses oreilles. Ainsi, il peut les orienter à sa guise.\nCe Pokémon peut entendre le plus discret des bruits.",
    height: "0,5 m",
    weight: "9,0 kg",
    category: "Vénépic",
    abilities: "Point Poison",
    color: Color(0xFF82708e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/032.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/032.png",
    evolutionChain: [32, 33, 34],
  ),
  Pokemon(
    id: 33,
    name: "Nidorino",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Nidorino dispose d'une corne plus dure que du diamant.\nS'il sent une présence hostile, toutes les pointes de son dos\nse hérissent d'un coup, puis il défie son ennemi.",
    height: "0,9 m",
    weight: "19,5 kg",
    category: "Vénépic",
    abilities: "Point Poison",
    color: Color(0xFF937b99),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/033.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/033.png",
    evolutionChain: [32, 33, 34],
  ),
  Pokemon(
    id: 34,
    name: "Nidoking",
    types: ["poison", "ground"],
    weaknesses: ["ground", "ice", "psychic", "water"],
    description:
        "L'épaisse queue de Nidoking est d'une puissance incroyable.\nEn un seul coup, il peut renverser une tour métallique.\nLorsque ce Pokémon se déchaîne, plus rien ne peut l'arrêter.",
    height: "1,4 m",
    weight: "62,0 kg",
    category: "Perceur",
    abilities: "Point Poison",
    color: Color(0xFF967e98),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/034.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/034.png",
    evolutionChain: [32, 33, 34],
  ),
  Pokemon(
    id: 35,
    name: "Mélofée",
    types: ["fairy"],
    weaknesses: ["steel", "poison"],
    description:
        "Les nuits de pleine lune, des groupes de ces Pokémon\nsortent jouer. Lorsque l'aube commence à poindre,\nles Mélofée fatigués rentrent dans leur retraite montagneuse\net vont dormir, blottis les uns contre les autres.",
    height: "0,6 m",
    weight: "7,5 kg",
    category: "Fée",
    abilities: "Joli Sourire",
    color: Color(0xFFdab7bc),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/035.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/035.png",
    evolutionChain: [35, 36],
  ),
  Pokemon(
    id: 36,
    name: "Mélodelfe",
    types: ["fairy"],
    weaknesses: ["steel", "poison"],
    description:
        "Les Mélodelfe se déplacent en sautant doucement,\ncomme s'ils volaient. Leur démarche légère leur permet\nmême de marcher sur l'eau. On raconte qu'ils se promènent\nsur les lacs, les soirs où la lune est claire.",
    height: "1,3 m",
    weight: "40,0 kg",
    category: "Fée",
    abilities: "Joli Sourire",
    color: Color(0xFFd8c0ba),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/036.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/036.png",
    evolutionChain: [35, 36],
  ),
  Pokemon(
    id: 37,
    name: "Goupix",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "À l'intérieur du corps de Goupix se trouve une flamme\nqui ne s'éteint jamais. Pendant la journée, lorsque\nla température augmente, ce Pokémon crache des flammes\npour éviter que son corps ne devienne trop chaud.",
    height: "0,6 m",
    weight: "9,9 kg",
    category: "Renard",
    abilities: "Rideau Neige",
    color: Color(0xFFb97b5e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/037.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/037.png",
    evolutionChain: [37, 38],
  ),
  Pokemon(
    id: 38,
    name: "Feunard",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "La légende raconte que Feunard est apparu lorsque\nneuf sorciers aux pouvoirs sacrés décidèrent de fusionner.\nCe Pokémon est très intelligent. Il comprend la langue\ndes hommes.",
    height: "1,1 m",
    weight: "19,9 kg",
    category: "Renard",
    abilities: "Rideau Neige",
    color: Color(0xFFd6ca99),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/038.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/038.png",
    evolutionChain: [37, 38],
  ),
  Pokemon(
    id: 39,
    name: "Rondoudou",
    types: ["normal", "fairy"],
    weaknesses: ["steel", "poison"],
    description:
        "Lorsque ce Pokémon chante, il ne s'arrête pas pour respirer.\nQuand il se bat contre un adversaire qu'il ne peut pas\nfacilement endormir, Rondoudou reste donc sans respirer,\nmettant sa vie en danger.",
    height: "0,5 m",
    weight: "5,5 kg",
    category: "Bouboule",
    abilities: "Joli Sourire",
    color: Color(0xFFc8b0b9),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/039.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/039.png",
    evolutionChain: [39, 40],
  ),
  Pokemon(
    id: 40,
    name: "Grodoudou",
    types: ["normal", "fairy"],
    weaknesses: ["steel", "poison"],
    description:
        "Le corps de Grodoudou est très élastique. S'il inspire\nprofondément, ce Pokémon peut se gonfler à volonté.\nUne fois gonflé, Grodoudou peut rebondir comme un ballon.",
    height: "1,0 m",
    weight: "12,0 kg",
    category: "Bouboule",
    abilities: "Joli Sourire",
    color: Color(0xFFcebfc5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/040.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/040.png",
    evolutionChain: [39, 40],
  ),
  Pokemon(
    id: 41,
    name: "Nosferapti",
    types: ["poison", "flying"],
    weaknesses: ["electric", "ice", "psychic", "rock"],
    description:
        "Nosferapti évite la lumière du soleil, car ça le rend malade.\nPendant la journée, il reste dans les cavernes ou à l'ombre\ndes vieilles maisons, où il dort, la tête à l'envers.",
    height: "0,8 m",
    weight: "7,5 kg",
    category: "Chovsouris",
    abilities: "Attention",
    color: Color(0xFF706587),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/041.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/041.png",
    evolutionChain: [41, 42],
  ),
  Pokemon(
    id: 42,
    name: "Nosferalto",
    types: ["poison", "flying"],
    weaknesses: ["electric", "ice", "psychic", "rock"],
    description:
        "Nosferalto mord sa proie grâce à ses quatre crocs pour\nboire son sang. Il ne sort que lorsque la nuit est noire\net sans lune, pour voleter en quête de gens et de Pokémon\nà mordre.",
    height: "1,6 m",
    weight: "55,0 kg",
    category: "Chovsouris",
    abilities: "Attention",
    color: Color(0xFF726488),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/042.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/042.png",
    evolutionChain: [41, 42],
  ),
  Pokemon(
    id: 43,
    name: "Mystherbe",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Mystherbe cherche un sol fertile et riche en nutriments,\npour s'y planter. Pendant la journée, quand il est planté,\nles pieds de ce Pokémon changent de forme et deviennent\nsimilaires à des racines.",
    height: "0,5 m",
    weight: "5,4 kg",
    category: "Racine",
    abilities: "Chlorophylle",
    color: Color(0xFF5a8d68),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/043.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/043.png",
    evolutionChain: [43, 44, 45],
  ),
  Pokemon(
    id: 44,
    name: "Ortide",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Ortide bave un miel qui sent horriblement mauvais.\nApparemment, il adore cette odeur nauséabonde.\nIl en renifle les fumées toxiques et se met à baver\ndu miel de plus belle.",
    height: "0,8 m",
    weight: "8,6 kg",
    category: "Racine",
    abilities: "Chlorophylle",
    color: Color(0xFF846c6c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/044.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/044.png",
    evolutionChain: [43, 44, 45],
  ),
  Pokemon(
    id: 45,
    name: "Rafflesia",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Rafflesia dispose des plus grands pétales du monde.\nIl s'en sert pour attirer ses proies avant de les endormir\navec ses spores toxiques. Ce Pokémon n'a plus alors\nqu'à attraper sa proie et à la manger.",
    height: "1,2 m",
    weight: "18,6 kg",
    category: "Fleur",
    abilities: "Chlorophylle",
    color: Color(0xFFab7887),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/045.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/045.png",
    evolutionChain: [43, 44, 45],
  ),
  Pokemon(
    id: 46,
    name: "Paras",
    types: ["bug", "grass"],
    weaknesses: ["fire", "flying", "bug", "ice", "poison", "rock"],
    description:
        "Paras accueille des champignons parasites appelés tochukaso\nqui poussent sur son dos. Ils grandissent grâce aux nutriments\ntrouvés sur le dos de ce Pokémon Insecte.\nIls peuvent rallonger l'espérance de vie.",
    height: "0,3 m",
    weight: "5,4 kg",
    category: "Champignon",
    abilities: "Pose Spore",
    color: Color(0xFFba856c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/046.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/046.png",
    evolutionChain: [46, 47],
  ),
  Pokemon(
    id: 47,
    name: "Parasect",
    types: ["bug", "grass"],
    weaknesses: ["fire", "flying", "bug", "ice", "poison", "rock"],
    description:
        "On sait que les Parasect vivent en groupe dans les grands\narbres et se nourrissent des nutriments contenus dans le tronc\net les racines. Lorsqu'un arbre infesté meurt, ils se précipitent\nvers le prochain.",
    height: "1,0 m",
    weight: "29,5 kg",
    category: "Champignon",
    abilities: "Pose Spore",
    color: Color(0xFFba806e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/047.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/047.png",
    evolutionChain: [46, 47],
  ),
  Pokemon(
    id: 48,
    name: "Mimitoss",
    types: ["bug", "poison"],
    weaknesses: ["fire", "flying", "psychic", "rock"],
    description:
        "On raconte que Mimitoss a évolué avec une fourrure de poils\nfins et drus qui protège son corps tout entier. Il est doté de\ngrands yeux capables de repérer ses proies, même minuscules.",
    height: "1,0 m",
    weight: "30,0 kg",
    category: "Vermine",
    abilities: "Œil Composé",
    color: Color(0xFF806f95),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/048.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/048.png",
    evolutionChain: [48, 49],
  ),
  Pokemon(
    id: 49,
    name: "Aéromite",
    types: ["bug", "poison"],
    weaknesses: ["fire", "flying", "psychic", "rock"],
    description:
        "Aéromite est un Pokémon nocturne, il ne sort donc que\nla nuit. Ses proies préférées sont les petits insectes qui\nse rassemblent autour des réverbères, attirés par la lumière.",
    height: "1,5 m",
    weight: "12,5 kg",
    category: "Papipoison",
    abilities: "Écran Poudre",
    color: Color(0xFFc0b6c9),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/049.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/049.png",
    evolutionChain: [48, 49],
  ),
  Pokemon(
    id: 50,
    name: "Taupiqueur",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "Les Taupiqueur sont élevés dans la plupart des fermes.\nEn effet, lorsque ce Pokémon creuse quelque part, le sol\nest comme labouré, prêt à recevoir les semences.\nOn peut alors y planter de délicieux légumes.",
    height: "0,2 m",
    weight: "1,0 kg",
    category: "Taupe",
    abilities: "Voile Sable",
    color: Color(0xFF9c877e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/050.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/050.png",
    evolutionChain: [50, 51],
  ),
  Pokemon(
    id: 51,
    name: "Triopikeur",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "Les Triopikeur sont en fait des triplés qui ont émergé du\nmême corps. C'est pourquoi chaque triplé pense exactement\ncomme les deux autres. Ils creusent inlassablement, dans\nune coopération parfaite.",
    height: "0,7 m",
    weight: "66,6 kg",
    category: "Taupe",
    abilities: "Voile Sable",
    color: Color(0xFF937f77),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/051.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/051.png",
    evolutionChain: [50, 51],
  ),
  Pokemon(
    id: 52,
    name: "Miaouss",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Miaouss peut rentrer ses griffes dans ses pattes pour rôder\ngracieusement sans laisser de traces. Étrangement,\nce Pokémon raffole des pièces d'or qui brillent à la lumière.",
    height: "0,4 m",
    weight: "4,2 kg",
    category: "Chadégout",
    abilities: "Ramassage",
    color: Color(0xFFb7a890),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/052.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/052.png",
    evolutionChain: [52, 53],
  ),
  Pokemon(
    id: 53,
    name: "Persian",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Persian a six grosses vibrisses qui lui donnent un air costaud\net lui permettent de sentir les mouvements de l'air pour savoir\nce qui se trouve à proximité. Il devient docile lorsqu'on\nl'attrape par les moustaches.",
    height: "1,1 m",
    weight: "33,0 kg",
    category: "Chadeville",
    abilities: "Technicien",
    color: Color(0xFFcbbb9d),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/053.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/053.png",
    evolutionChain: [52, 53],
  ),
  Pokemon(
    id: 54,
    name: "Psykokwak",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Lorsqu'il utilise son mystérieux pouvoir, Psykokwak\nne s'en souvient pas. Apparemment, il ne peut pas garder\nce genre d'événement en mémoire, car il pratique ce pouvoir\ndans un état proche du sommeil profond.",
    height: "0,8 m",
    weight: "19,6 kg",
    category: "Canard",
    abilities: "Moiteur",
    color: Color(0xFFd8bf92),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/054.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/054.png",
    evolutionChain: [54, 55],
  ),
  Pokemon(
    id: 55,
    name: "Akwakwak",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Akwakwak est le Pokémon qui nage le plus vite.\nIl nage sans se fatiguer, même lorsque la mer est agitée.\nIl sauve parfois des gens coincés dans les navires bloqués\nen haute mer.",
    height: "1,7 m",
    weight: "76,6 kg",
    category: "Canard",
    abilities: "Moiteur",
    color: Color(0xFF7297b2),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/055.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/055.png",
    evolutionChain: [54, 55],
  ),
  Pokemon(
    id: 56,
    name: "Férosinge",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Lorsque Férosinge commence à trembler et que sa respiration\ndevient haletante, cela signifie qu'il est en colère. En outre,\nla moutarde lui monte au nez tellement vite qu'il est presque\nimpossible d'échapper à sa colère.",
    height: "0,5 m",
    weight: "28,0 kg",
    category: "Porsinge",
    abilities: "Esprit Vital",
    color: Color(0xFFc3b0a5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/056.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/056.png",
    evolutionChain: [56, 57],
  ),
  Pokemon(
    id: 57,
    name: "Colossinge",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Lorsque Colossinge devient furieux, sa circulation sanguine\ns'accélère. Du coup, ses muscles sont encore plus puissants.\nEn revanche, il devient bien moins intelligent.",
    height: "1,0 m",
    weight: "32,0 kg",
    category: "Porsinge",
    abilities: "Esprit Vital",
    color: Color(0xFFa79288),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/057.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/057.png",
    evolutionChain: [56, 57],
  ),
  Pokemon(
    id: 58,
    name: "Caninos",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "Caninos a un odorat très développé. Ce Pokémon n'oublie\njamais un parfum, quel qu'il soit. Il utilise son puissant sens\nolfactif pour deviner les émotions des autres créatures\nvivantes.",
    height: "0,7 m",
    weight: "19,0 kg",
    category: "Chiot",
    abilities: "Intimidation",
    color: Color(0xFFbea186),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/058.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/058.png",
    evolutionChain: [58, 59],
  ),
  Pokemon(
    id: 59,
    name: "Arcanin",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "Arcanin est célèbre pour son extraordinaire vitesse.\nOn le dit capable de parcourir plus de 10 000 km en 24 h.\nLe feu qui fait rage à l'intérieur du corps de ce Pokémon\nest la source de son pouvoir.",
    height: "1,9 m",
    weight: "155,0 kg",
    category: "Légendaire",
    abilities: "Intimidation",
    color: Color(0xFFbea38b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/059.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/059.png",
    evolutionChain: [58, 59],
  ),
  Pokemon(
    id: 60,
    name: "Ptitard",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Ptitard a une peau très fine. On peut même voir les\nentrailles en spirale de ce Pokémon à travers sa peau.\nMalgré sa finesse, cette peau est aussi très élastique.\nMême les crocs les plus acérés rebondissent dessus.",
    height: "0,6 m",
    weight: "12,4 kg",
    category: "Têtard",
    abilities: "Moiteur",
    color: Color(0xFF8294ab),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/060.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/060.png",
    evolutionChain: [60, 61, 62],
  ),
  Pokemon(
    id: 61,
    name: "Têtarte",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "La peau de Têtarte est toujours maintenue humide par\nun liquide huileux. Grâce à cette protection graisseuse,\nil peut facilement se glisser hors de l'étreinte de n'importe\nquel ennemi.",
    height: "1,0 m",
    weight: "20,0 kg",
    category: "Têtard",
    abilities: "Moiteur",
    color: Color(0xFF8294aa),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/061.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/061.png",
    evolutionChain: [60, 61, 62],
  ),
  Pokemon(
    id: 62,
    name: "Tartard",
    types: ["water", "fighting"],
    weaknesses: ["electric", "flying", "grass", "psychic", "fairy"],
    description:
        "Les muscles solides et surdéveloppés de Tartard ne se\nfatiguent jamais, quels que soient les efforts qu'il produit.\nCe Pokémon est tellement endurant qu'il peut traverser\nun océan à la nage avec une étonnante facilité.",
    height: "1,3 m",
    weight: "54,0 kg",
    category: "Têtard",
    abilities: "Moiteur",
    color: Color(0xFF8094ae),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/062.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/062.png",
    evolutionChain: [60, 61, 62],
  ),
  Pokemon(
    id: 63,
    name: "Abra",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Abra doit dormir dix-huit heures par jour. S'il dort moins,\nce Pokémon ne peut plus utiliser ses pouvoirs télékinétiques.\nLorsqu'il est attaqué, Abra s'enfuit en utilisant Téléport,\nsans même se réveiller.",
    height: "0,9 m",
    weight: "19,5 kg",
    category: "Psy",
    abilities: "Attention",
    color: Color(0xFFbca95a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/063.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/063.png",
    evolutionChain: [63, 64, 65],
  ),
  Pokemon(
    id: 64,
    name: "Kadabra",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Kadabra tient une cuiller en argent dans la main. Elle est\nutilisée pour amplifier les ondes alpha de son cerveau.\nSans elle, on raconte que ce Pokémon ne peut utiliser que\nla moitié de ses pouvoirs télékinétiques.",
    height: "1,3 m",
    weight: "56,5 kg",
    category: "Psy",
    abilities: "Attention",
    color: Color(0xFFb09c58),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/064.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/064.png",
    evolutionChain: [63, 64, 65],
  ),
  Pokemon(
    id: 65,
    name: "Alakazam",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Le cerveau d'Alakazam grossit sans arrêt, multipliant sans\ncesse ses cellules. Ce Pokémon a un QI incroyablement\nélevé, de 5 000. Il peut garder en mémoire tout ce qui\ns'est passé dans le monde.",
    height: "1,2 m",
    weight: "48,0 kg",
    category: "Psy",
    abilities: "Calque",
    color: Color(0xFFb1a163),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/065.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/065.png",
    evolutionChain: [63, 64, 65],
  ),
  Pokemon(
    id: 66,
    name: "Machoc",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Machoc s'entraîne en soulevant un Gravalanch, comme\ns'il s'agissait d'haltères. Certains Machoc voyagent un peu\npartout dans le monde pour apprendre à maîtriser tous\nles types d'arts martiaux.",
    height: "0,8 m",
    weight: "19,5 kg",
    category: "Colosse",
    abilities: "Cran",
    color: Color(0xFF94a7ae),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/066.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/066.png",
    evolutionChain: [66, 67, 68],
  ),
  Pokemon(
    id: 67,
    name: "Machopeur",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Machopeur pratique le body-building tous les jours,\nmême lorsqu'il aide les gens à réaliser de durs travaux.\nPendant ses congés, ce Pokémon va s'entraîner dans\nles champs et les montagnes.",
    height: "1,5 m",
    weight: "70,5 kg",
    category: "Colosse",
    abilities: "Cran",
    color: Color(0xFF9192a6),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/067.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/067.png",
    evolutionChain: [66, 67, 68],
  ),
  Pokemon(
    id: 68,
    name: "Mackogneur",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Mackogneur est célèbre, car c'est le seul Pokémon qui\na réussi à maîtriser tous les types d'arts martiaux.\nS'il parvient à attraper son ennemi avec ses quatre bras,\nil le projette par-delà l'horizon.",
    height: "1,6 m",
    weight: "130,0 kg",
    category: "Colosse",
    abilities: "Cran",
    color: Color(0xFF85909c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/068.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/068.png",
    evolutionChain: [66, 67, 68],
  ),
  Pokemon(
    id: 69,
    name: "Chétiflor",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Le corps long et flexible de Chétiflor lui permet de se tordre\net d'osciller pour éviter tout type d'attaque, même les plus\npuissantes. Ce Pokémon crache un fluide corrosif qui peut\nmême dissoudre le fer.",
    height: "0,7 m",
    weight: "4,0 kg",
    category: "Fleur",
    abilities: "Chlorophylle",
    color: Color(0xFFa3b47b),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/069.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/069.png",
    evolutionChain: [69, 70, 71],
  ),
  Pokemon(
    id: 70,
    name: "Boustiflor",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Boustiflor est doté d'un gros crochet. La nuit, ce Pokémon\ns'accroche à une branche pour s'endormir. Quand il a\nun sommeil agité, il se réveille par terre.",
    height: "1,0 m",
    weight: "6,4 kg",
    category: "Carnivore",
    abilities: "Chlorophylle",
    color: Color(0xFFa5b37f),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/070.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/070.png",
    evolutionChain: [69, 70, 71],
  ),
  Pokemon(
    id: 71,
    name: "Empiflor",
    types: ["grass", "poison"],
    weaknesses: ["fire", "flying", "ice", "psychic"],
    description:
        "Empiflor est doté d'une longue liane qui part de sa tête.\nCette liane se balance et remue comme un animal pour attirer\nses proies. Lorsque l'une d'elles s'approche un peu trop près,\nce Pokémon l'avale entièrement.",
    height: "1,7 m",
    weight: "15,5 kg",
    category: "Carnivore",
    abilities: "Chlorophylle",
    color: Color(0xFF97b074),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/071.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/071.png",
    evolutionChain: [69, 70, 71],
  ),
  Pokemon(
    id: 72,
    name: "Tentacool",
    types: ["water", "poison"],
    weaknesses: ["electric", "ground", "psychic"],
    description:
        "Grâce à l'eau de son corps, Tentacool convertit la lumière\ndu soleil absorbée et la renvoie sous forme de rayon\nd'énergie. Ce Pokémon lance ces rayons par le petit\norgane rond situé au-dessus de ses yeux.",
    height: "0,9 m",
    weight: "45,5 kg",
    category: "Mollusque",
    abilities: "Corps Sain",
    color: Color(0xFF6f7d8f),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/072.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/072.png",
    evolutionChain: [72, 73],
  ),
  Pokemon(
    id: 73,
    name: "Tentacruel",
    types: ["water", "poison"],
    weaknesses: ["electric", "ground", "psychic"],
    description:
        "Les tentacules de Tentacruel peuvent s'allonger et se rétracter\nà volonté. Il serre sa proie dans ses tentacules et l'affaiblit\nen lui injectant une toxine. Il peut attraper jusqu'à 80 proies\nen même temps.",
    height: "1,6 m",
    weight: "55,0 kg",
    category: "Mollusque",
    abilities: "Corps Sain",
    color: Color(0xFF778190),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/073.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/073.png",
    evolutionChain: [72, 73],
  ),
  Pokemon(
    id: 74,
    name: "Racaillou",
    types: ["rock", "ground"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "Pour dormir profondément, Racaillou s'enterre à moitié\ndans le sol. Il ne se réveille pas, même si des randonneurs\nlui marchent dessus. Au petit matin, ce Pokémon roule en\ncontrebas en quête de nourriture.",
    height: "0,4 m",
    weight: "20,3 kg",
    category: "Roche",
    abilities: "Fermeté",
    color: Color(0xFF969490),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/074.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/074.png",
    evolutionChain: [74, 75, 76],
  ),
  Pokemon(
    id: 75,
    name: "Gravalanch",
    types: ["rock", "ground"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "La nourriture préférée de Gravalanch est la roche.\nCe Pokémon escalade parfois les montagnes, dévorant\nles rochers sur son passage. Une fois au sommet,\nil se laisse rouler jusqu'en bas.",
    height: "1,0 m",
    weight: "110,0 kg",
    category: "Roche",
    abilities: "Fermeté",
    color: Color(0xFF959791),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/075.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/075.png",
    evolutionChain: [74, 75, 76],
  ),
  Pokemon(
    id: 76,
    name: "Grolem",
    types: ["rock", "ground"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "On sait que les Grolem se laissent rouler en bas\ndes montagnes. Afin d'éviter qu'ils roulent sur les maisons\ndes gens, des tranchées ont été creusées le long des\nmontagnes pour les guider dans leurs descentes infernales.",
    height: "1,7 m",
    weight: "316,0 kg",
    category: "Titanesque",
    abilities: "Fermeté",
    color: Color(0xFF797372),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/076.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/076.png",
    evolutionChain: [74, 75, 76],
  ),
  Pokemon(
    id: 77,
    name: "Ponyta",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "À sa naissance, Ponyta est très faible. Il peut à peine tenir\ndebout. Ce Pokémon se muscle en trébuchant et en tombant,\nlorsqu'il essaie de suivre ses parents.",
    height: "1,0 m",
    weight: "30,0 kg",
    category: "Cheval Feu",
    abilities: "Fuite",
    color: Color(0xFFddb380),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/077.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/077.png",
    evolutionChain: [77, 78],
  ),
  Pokemon(
    id: 78,
    name: "Galopa",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "On voit souvent Galopa trotter dans les champs et les plaines.\nCependant, lorsque ce Pokémon s'en donne la peine, il peut\ngaloper à plus de 240 km/h et sa crinière flamboyante\ns'embrase.",
    height: "1,7 m",
    weight: "95,0 kg",
    category: "Cheval Feu",
    abilities: "Fuite",
    color: Color(0xFFdeae84),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/078.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/078.png",
    evolutionChain: [77, 78],
  ),
  Pokemon(
    id: 79,
    name: "Ramoloss",
    types: ["water", "psychic"],
    weaknesses: ["bug", "dark", "electric", "ghost", "grass"],
    description:
        "Ramoloss trempe sa queue dans l'eau au bord des rivières\npour attraper ses proies. Cependant, ce Pokémon oublie\nsouvent ce qu'il fait là et passe des jours entiers à traîner\nau bord de l'eau.",
    height: "1,2 m",
    weight: "36,0 kg",
    category: "Crétin",
    abilities: "Benêt",
    color: Color(0xFFcea1ac),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/079.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/079.png",
    evolutionChain: [79, 80],
  ),
  Pokemon(
    id: 80,
    name: "Flagadoss",
    types: ["water", "psychic"],
    weaknesses: ["bug", "dark", "electric", "ghost", "grass"],
    description:
        "Flagadoss a un Kokiyas solidement attaché à sa queue.\nDu coup, il ne peut plus l'utiliser pour pêcher. Flagadoss est\ndonc obligé, à contrecœur, de nager pour attraper ses proies.",
    height: "2,0 m",
    weight: "120,0 kg",
    category: "Symbiose",
    abilities: "Coque Armure",
    color: Color(0xFFbba1a7),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/080.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/080.png",
    evolutionChain: [79, 80],
  ),
  Pokemon(
    id: 81,
    name: "Magnéti",
    types: ["electric", "steel"],
    weaknesses: ["ground", "fire", "fighting"],
    description:
        "Magnéti flotte dans les airs en émettant des ondes\nélectromagnétiques par les aimants sur ses côtés.\nCes ondes annulent les effets de la gravité. Ce Pokémon\nne peut plus voler si son stock d'électricité est épuisé.",
    height: "0,3 m",
    weight: "6,0 kg",
    category: "Magnétique",
    abilities: "Fermeté",
    color: Color(0xFF979fa5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/081.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/081.png",
    evolutionChain: [81, 82],
  ),
  Pokemon(
    id: 82,
    name: "Magnéton",
    types: ["electric", "steel"],
    weaknesses: ["ground", "fire", "fighting"],
    description:
        "Magnéton émet un puissant champ magnétique qui neutralise\nles appareils électroniques. Certaines villes demandent\naux propriétaires de ces Pokémon de les garder dans\nleurs Poké Balls.",
    height: "1,0 m",
    weight: "60,0 kg",
    category: "Magnétique",
    abilities: "Fermeté",
    color: Color(0xFF91979c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/082.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/082.png",
    evolutionChain: [81, 82],
  ),
  Pokemon(
    id: 83,
    name: "Canarticho",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "On voit souvent des Canarticho avec une tige, récupérée sur\nune plante quelconque. Apparemment, ils peuvent distinguer\nles bonnes des mauvaises. On a vu ces Pokémon se battre\npour des histoires de tiges.",
    height: "0,8 m",
    weight: "15,0 kg",
    category: "Canard Fou",
    abilities: "Regard Vif",
    color: Color(0xFFa0947e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/083.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/083.png",
    evolutionChain: [83],
  ),
  Pokemon(
    id: 84,
    name: "Doduo",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Les deux têtes de Doduo contiennent des cerveaux\ntotalement identiques. Une étude scientifique démontra\nque dans des cas rares, certains de ces Pokémon\npossèdent des cerveaux différents.",
    height: "1,4 m",
    weight: "39,2 kg",
    category: "Duoiseau",
    abilities: "Fuite",
    color: Color(0xFFa68670),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/084.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/084.png",
    evolutionChain: [84, 85],
  ),
  Pokemon(
    id: 85,
    name: "Dodrio",
    types: ["normal", "flying"],
    weaknesses: ["electric", "ice", "rock"],
    description:
        "Apparemment, Dodrio ne se contente pas d'avoir trois têtes.\nIl semble également avoir trois cœurs et six poumons,\nce qui lui permet de courir très longtemps sans s'épuiser.",
    height: "1,8 m",
    weight: "85,2 kg",
    category: "Trioiseau",
    abilities: "Fuite",
    color: Color(0xFF9a7d71),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/085.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/085.png",
    evolutionChain: [84, 85],
  ),
  Pokemon(
    id: 86,
    name: "Otaria",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Otaria chasse ses proies dans l'eau gelée, sous la couche de\nglace. Lorsqu'il cherche à respirer, il perce un trou en frappant\nla glace avec la partie saillante de sa tête.",
    height: "1,1 m",
    weight: "90,0 kg",
    category: "Otarie",
    abilities: "Isograisse",
    color: Color(0xFFc9cfd6),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/086.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/086.png",
    evolutionChain: [86, 87],
  ),
  Pokemon(
    id: 87,
    name: "Lamantine",
    types: ["water", "ice"],
    weaknesses: ["electric", "fighting", "grass", "rock"],
    description:
        "Lamantine adore piquer un roupillon à même la glace. Il y a\ntrès longtemps, un marin ayant aperçu ce Pokémon dormant\nsur un glacier a cru voir une sirène.",
    height: "1,7 m",
    weight: "120,0 kg",
    category: "Otarie",
    abilities: "Isograisse",
    color: Color(0xFFd3d8e1),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/087.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/087.png",
    evolutionChain: [86, 87],
  ),
  Pokemon(
    id: 88,
    name: "Tadmorv",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Tadmorv est apparu dans la vase accumulée sur un bord\nde mer pollué. Ce Pokémon aime tout ce qui est dégoûtant.\nUne substance pleine de germes suinte constamment\nde tout son corps.",
    height: "0,7 m",
    weight: "42,0 kg",
    category: "Dégueu",
    abilities: "Gloutonnerie",
    color: Color(0xFF8d7b96),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/088.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/088.png",
    evolutionChain: [88, 89],
  ),
  Pokemon(
    id: 89,
    name: "Grotadmorv",
    types: ["poison"],
    weaknesses: ["ground", "psychic"],
    description:
        "Ce Pokémon ne mange que ce qui est répugnant et abject.\nLes Grotadmorv se rassemblent dans les villes où les gens\njettent tout par terre, dans la rue.",
    height: "1,0 m",
    weight: "52,0 kg",
    category: "Dégueu",
    abilities: "Gloutonnerie",
    color: Color(0xFF84788a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/089.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/089.png",
    evolutionChain: [88, 89],
  ),
  Pokemon(
    id: 90,
    name: "Kokiyas",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "La nuit, ce Pokémon utilise sa grande langue pour creuser\nun trou dans le sable des fonds marins afin d'y dormir.\nUne fois endormi, Kokiyas referme sa coquille, mais laisse\nsa langue dépasser.",
    height: "0,3 m",
    weight: "4,0 kg",
    category: "Bivalve",
    abilities: "Coque Armure",
    color: Color(0xFF7e7690),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/090.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/090.png",
    evolutionChain: [90, 91],
  ),
  Pokemon(
    id: 91,
    name: "Crustabri",
    types: ["water", "ice"],
    weaknesses: ["electric", "fighting", "grass", "rock"],
    description:
        "Crustabri est capable de se déplacer dans les fonds marins\nen avalant de l'eau et en la rejetant vers l'arrière.\nCe Pokémon envoie des pointes en utilisant la même méthode.",
    height: "1,5 m",
    weight: "132,5 kg",
    category: "Bivalve",
    abilities: "Coque Armure",
    color: Color(0xFF757085),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/091.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/091.png",
    evolutionChain: [90, 91],
  ),
  Pokemon(
    id: 92,
    name: "Fantominus",
    types: ["ghost", "poison"],
    weaknesses: ["dark", "ghost", "psychic"],
    description:
        "Fantominus est principalement constitué de matière gazeuse.\nLorsqu'il est exposé au vent, son corps gazeux se disperse\net diminue. Des groupes de ce Pokémon se rassemblent sous\nles auvents des maisons pour se protéger.",
    height: "1,3 m",
    weight: "0,1 kg",
    category: "Gaz",
    abilities: "Lévitation",
    color: Color(0xFF786875),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/092.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/092.png",
    evolutionChain: [92, 93, 94],
  ),
  Pokemon(
    id: 93,
    name: "Spectrum",
    types: ["ghost", "poison"],
    weaknesses: ["dark", "ghost", "psychic"],
    description:
        "Spectrum est un Pokémon dangereux. Si l'un d'entre eux fait\nsigne d'approcher, il ne faut jamais l'écouter. Ce Pokémon\nrisque de sortir sa langue pour essayer de voler votre vie.",
    height: "1,6 m",
    weight: "0,1 kg",
    category: "Gaz",
    abilities: "Lévitation",
    color: Color(0xFF6d6276),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/093.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/093.png",
    evolutionChain: [92, 93, 94],
  ),
  Pokemon(
    id: 94,
    name: "Ectoplasma",
    types: ["ghost", "poison"],
    weaknesses: ["dark", "ghost", "psychic"],
    description:
        "Parfois, pendant les nuits noires, une ombre projetée par\nun réverbère peut tout à coup vous dépasser. Il s'agit d'un\nEctoplasma qui court, en se faisant passer pour l'ombre\nde quelqu'un d'autre.",
    height: "1,4 m",
    weight: "40,5 kg",
    category: "Ombre",
    abilities: "Marque Ombre",
    color: Color(0xFF7c7694),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/094.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/094.png",
    evolutionChain: [92, 93, 94],
  ),
  Pokemon(
    id: 95,
    name: "Onix",
    types: ["rock", "ground"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "Onix a dans le cerveau un aimant qui lui sert de boussole.\nIl permet à ce Pokémon de ne pas se perdre pendant\nqu'il creuse. En prenant de l'âge, son corps s'arrondit\net se polit.",
    height: "8,8 m",
    weight: "210,0 kg",
    category: "Serpenroc",
    abilities: "Tête de Roc",
    color: Color(0xFF7a7983),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/095.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/095.png",
    evolutionChain: [95],
  ),
  Pokemon(
    id: 96,
    name: "Soporifik",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Lorsque les enfants ont le nez qui les démange en dormant,\nc'est sans doute parce que ce Pokémon se tient au-dessus\nde leur oreiller, afin d'essayer de manger leurs rêves par\nleurs narines.",
    height: "1,0 m",
    weight: "32,4 kg",
    category: "Hypnose",
    abilities: "Insomnia",
    color: Color(0xFFaa9459),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/096.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/096.png",
    evolutionChain: [96, 97],
  ),
  Pokemon(
    id: 97,
    name: "Hypnomade",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Hypnomade tient un pendule dans sa main. Le mouvement\nde balancier et les reflets brillants du pendule hypnotisent\nprofondément son ennemi. Lorsque ce Pokémon cherche\nses proies, il nettoie son pendule.",
    height: "1,6 m",
    weight: "75,6 kg",
    category: "Hypnose",
    abilities: "Insomnia",
    color: Color(0xFFcdb964),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/097.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/097.png",
    evolutionChain: [96, 97],
  ),
  Pokemon(
    id: 98,
    name: "Krabby",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Krabby vit sur les plages, enterré dans le sable. Sur les plages\noù on trouve peu de nourriture, on peut voir ces Pokémon\nse disputer pour défendre leur territoire.",
    height: "0,4 m",
    weight: "6,5 kg",
    category: "Doux Crabe",
    abilities: "Coque Armure",
    color: Color(0xFFb9907d),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/098.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/098.png",
    evolutionChain: [98, 99],
  ),
  Pokemon(
    id: 99,
    name: "Krabboss",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Krabboss est doté d'une pince gigantesque, surdimensionnée.\nIl l'agite en l'air pour communiquer avec ses semblables.\nEn revanche, sa pince est tellement lourde que ce Pokémon\nse fatigue très vite.",
    height: "1,3 m",
    weight: "60,0 kg",
    category: "Pince",
    abilities: "Coque Armure",
    color: Color(0xFFb58a76),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/099.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/099.png",
    evolutionChain: [98, 99],
  ),
  Pokemon(
    id: 100,
    name: "Voltorbe",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "Voltorbe est extrêmement sensible. Il explose au moindre\nchoc. On raconte qu'il fut créé par l'exposition d'une\nPoké Ball à une puissante dose d'énergie.",
    height: "0,5 m",
    weight: "10,4 kg",
    category: "Balle",
    abilities: "Statik",
    color: Color(0xFFc59da0),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/100.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/100.png",
    evolutionChain: [100, 101],
  ),
  Pokemon(
    id: 101,
    name: "Électrode",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "L'une des caractéristiques d'Électrode est son attirance\npour l'électricité. Ces Pokémon posent problème lorsqu'ils\nse rassemblent dans les centrales électriques pour se nourrir\nde courant fraîchement généré.",
    height: "1,2 m",
    weight: "66,6 kg",
    category: "Balle",
    abilities: "Statik",
    color: Color(0xFFbfb5be),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/101.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/101.png",
    evolutionChain: [100, 101],
  ),
  Pokemon(
    id: 102,
    name: "Noeunoeuf",
    types: ["grass", "psychic"],
    weaknesses: ["bug", "dark", "fire", "flying", "ghost", "ice", "poison"],
    description:
        "Ce Pokémon est constitué de six œufs formant une grappe\nserrée. Ces six œufs s'attirent mutuellement et pivotent.\nQuand des fissures apparaissent sur les coquilles, ça signifie\nque Noeunoeuf est sur le point d'évoluer.",
    height: "0,4 m",
    weight: "2,5 kg",
    category: "Œuf",
    abilities: "Chlorophylle",
    color: Color(0xFFd4bdbf),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/102.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/102.png",
    evolutionChain: [102, 103],
  ),
  Pokemon(
    id: 103,
    name: "Noadkoko",
    types: ["grass", "psychic"],
    weaknesses: ["bug", "dark", "fire", "flying", "ghost", "ice", "poison"],
    description:
        "Noadkoko vient des tropiques. À force de vivre sous un soleil\nardent, ses têtes ont rapidement grandi. On raconte que\nlorsque ses têtes tombent, elles se rassemblent et forment\nun Noeunoeuf.",
    height: "10,9 m",
    weight: "415,6 kg",
    category: "Fruitpalme",
    abilities: "Fouille",
    color: Color(0xFF879b7c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/103.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/103.png",
    evolutionChain: [102, 103],
  ),
  Pokemon(
    id: 104,
    name: "Osselait",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "La maman d'Osselait lui manque terriblement et il ne la reverra\njamais. La lune le fait pleurer, car elle lui rappelle sa mère.\nLes taches sur le crâne que porte ce Pokémon sont\nles marques de ses larmes.",
    height: "0,4 m",
    weight: "6,5 kg",
    category: "Solitaire",
    abilities: "Tête de Roc",
    color: Color(0xFFb6aea2),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/104.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/104.png",
    evolutionChain: [104, 105],
  ),
  Pokemon(
    id: 105,
    name: "Ossatueur",
    types: ["ground"],
    weaknesses: ["grass", "ice", "water"],
    description:
        "Ossatueur est la forme évoluée d'Osselait. Il a surmonté\nle chagrin causé par la perte de sa maman et s'est endurci.\nLe tempérament décidé et entier de ce Pokémon le rend\ntrès difficile à amadouer.",
    height: "1,0 m",
    weight: "34,0 kg",
    category: "Gard'Os",
    abilities: "Paratonnerre",
    color: Color(0xFFb1a69a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/105.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/105.png",
    evolutionChain: [104, 105],
  ),
  Pokemon(
    id: 106,
    name: "Kicklee",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "Les jambes de Kicklee peuvent se contracter et s'étirer\nà volonté. Grâce à ces jambes à ressort, il terrasse ses\nennemis en les rouant de coups de pied. Après les combats,\nil masse ses jambes pour éviter de sentir la fatigue.",
    height: "1,5 m",
    weight: "49,8 kg",
    category: "Latteur",
    abilities: "Échauffement",
    color: Color(0xFFa69791),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/106.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/106.png",
    evolutionChain: [106, 107],
  ),
  Pokemon(
    id: 107,
    name: "Tygnon",
    types: ["fighting"],
    weaknesses: ["flying", "psychic", "fairy"],
    description:
        "On raconte que Tygnon dispose de l'état d'esprit d'un boxeur\nqui s'entraîne pour le championnat du monde. Ce Pokémon\nest doté d'une ténacité à toute épreuve et n'abandonne\njamais face à l'adversité.",
    height: "1,4 m",
    weight: "50,2 kg",
    category: "Puncheur",
    abilities: "Regard Vif",
    color: Color(0xFFa18c8a),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/107.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/107.png",
    evolutionChain: [106, 107],
  ),
  Pokemon(
    id: 108,
    name: "Excelangue",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Chaque fois qu'Excelangue découvre quelque chose\nde nouveau, il le lèche. Sa mémoire est basée sur le goût\net la texture des objets. Il n'aime pas les choses acides.",
    height: "1,2 m",
    weight: "65,5 kg",
    category: "Lécheur",
    abilities: "Benêt",
    color: Color(0xFFcca3a7),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/108.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/108.png",
    evolutionChain: [108],
  ),
  Pokemon(
    id: 109,
    name: "Smogo",
    types: ["poison"],
    weaknesses: ["psychic"],
    description:
        "Smogo est composé de substances toxiques. Il mélange\ndes toxines et des détritus pour déclencher une réaction\nchimique générant un gaz très dangereux. Plus la température\nest élevée, plus la quantité de gaz est importante.",
    height: "0,6 m",
    weight: "1,0 kg",
    category: "Gaz Mortel",
    abilities: "Lévitation",
    color: Color(0xFF9e9a9f),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/109.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/109.png",
    evolutionChain: [109, 110],
  ),
  Pokemon(
    id: 110,
    name: "Smogogo",
    types: ["poison"],
    weaknesses: ["psychic"],
    description:
        "Smogogo rétrécit ou gonfle ses deux corps pour mélanger\nles gaz toxiques qui s'y trouvent. Lorsque les gaz sont bien\nmélangés, la toxine devient très puissante. Le Pokémon\nse putréfie aussi un peu plus.",
    height: "1,2 m",
    weight: "9,5 kg",
    category: "Gaz Mortel",
    abilities: "Lévitation",
    color: Color(0xFF948b95),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/110.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/110.png",
    evolutionChain: [109, 110],
  ),
  Pokemon(
    id: 111,
    name: "Rhinocorne",
    types: ["ground", "rock"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "Le cerveau de Rhinocorne est tout petit, à tel point que\nlorsqu'il attaque, il lui arrive d'oublier pourquoi il a commencé\nà charger. Il lui arrive de se souvenir qu'il a démoli certaines\nchoses.",
    height: "1,0 m",
    weight: "115,0 kg",
    category: "Piquant",
    abilities: "Tête de Roc",
    color: Color(0xFF777f88),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/111.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/111.png",
    evolutionChain: [111, 112],
  ),
  Pokemon(
    id: 112,
    name: "Rhinoféros",
    types: ["ground", "rock"],
    weaknesses: ["grass", "water", "fighting", "ground", "ice", "steel"],
    description:
        "La corne de Rhinoféros lui sert de foreuse. Il l'utilise pour\ndétruire des rochers et des cailloux. Ce Pokémon charge\nde temps en temps dans du magma en fusion, mais sa peau\nblindée le protège de la chaleur.",
    height: "1,9 m",
    weight: "120,0 kg",
    category: "Perceur",
    abilities: "Tête de Roc",
    color: Color(0xFF8d8f97),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/112.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/112.png",
    evolutionChain: [111, 112],
  ),
  Pokemon(
    id: 113,
    name: "Leveinard",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Leveinard pond tous les jours des œufs pleins de vitamines.\nCes œufs sont tellement bons que les gens les mangent\nmême quand ils n'ont pas faim.",
    height: "1,1 m",
    weight: "34,6 kg",
    category: "Œuf",
    abilities: "Médic Nature",
    color: Color(0xFFd8b4c2),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/113.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/113.png",
    evolutionChain: [113],
  ),
  Pokemon(
    id: 114,
    name: "Saquedeneu",
    types: ["grass"],
    weaknesses: ["bug", "fire", "flying", "ice", "poison"],
    description:
        "Les lianes de Saquedeneu se brisent facilement lorsqu'on\nles attrape. Cela ne lui fait pas mal et lui permet simplement\nde s'échapper rapidement. Les lianes cassées repoussent\nle lendemain.",
    height: "1,0 m",
    weight: "35,0 kg",
    category: "Vigne",
    abilities: "Chlorophylle",
    color: Color(0xFF677484),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/114.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/114.png",
    evolutionChain: [114],
  ),
  Pokemon(
    id: 115,
    name: "Kangourex",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Lorsqu'on rencontre un petit Kangourex qui joue tout seul,\nil ne faut jamais le déranger ou essayer de l'attraper.\nLes parents du bébé Pokémon sont sûrement dans le coin\net ils risquent d'entrer dans une colère noire.",
    height: "2,2 m",
    weight: "100,0 kg",
    category: "Maternel",
    abilities: "Amour Filial",
    color: Color(0xFFa79689),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/115.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/115.png",
    evolutionChain: [115],
  ),
  Pokemon(
    id: 116,
    name: "Hypotrempe",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Lorsque Hypotrempe sent un danger, il libère instinctivement\nune épaisse encre noire pour pouvoir s'échapper.\nCe Pokémon nage en agitant sa nageoire dorsale.",
    height: "0,4 m",
    weight: "8,0 kg",
    category: "Dragon",
    abilities: "Glissade",
    color: Color(0xFF89bac5),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/116.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/116.png",
    evolutionChain: [116, 117],
  ),
  Pokemon(
    id: 117,
    name: "Hypocéan",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Hypocéan déclenche des tourbillons en faisant tournoyer\nson corps. Ces tourbillons sont assez puissants pour engloutir\ndes bateaux de pêche. Ce Pokémon affaiblit sa proie grâce à\nces courants, puis l'avale en une bouchée.",
    height: "1,2 m",
    weight: "25,0 kg",
    category: "Dragon",
    abilities: "Point Poison",
    color: Color(0xFF7eafbd),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/117.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/117.png",
    evolutionChain: [116, 117],
  ),
  Pokemon(
    id: 118,
    name: "Poissirène",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Poissirène adore nager librement dans les rivières et\nles étangs. Si l'un de ces Pokémon est mis dans un aquarium,\nil n'hésitera pas à casser la vitre avec sa puissante corne pour\ns'enfuir.",
    height: "0,6 m",
    weight: "15,0 kg",
    category: "Poisson",
    abilities: "Glissade",
    color: Color(0xFFcdbdc1),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/118.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/118.png",
    evolutionChain: [118, 119],
  ),
  Pokemon(
    id: 119,
    name: "Poissoroy",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Les Poissoroy font tout pour protéger leurs œufs.\nLes mâles et les femelles patrouillent pour surveiller le nid et\nles œufs. La garde de ces œufs dure un peu plus d'un mois.",
    height: "1,3 m",
    weight: "39,0 kg",
    category: "Poisson",
    abilities: "Glissade",
    color: Color(0xFFbba9a7),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/119.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/119.png",
    evolutionChain: [118, 119],
  ),
  Pokemon(
    id: 120,
    name: "Stari",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Stari communique apparemment avec les étoiles dans\nle ciel en faisant clignoter son cœur rouge. Si des parties\nde son corps sont cassées, ce Pokémon les régénère.",
    height: "0,8 m",
    weight: "34,5 kg",
    category: "Étoile",
    abilities: "Médic Nature",
    color: Color(0xFF988360),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/120.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/120.png",
    evolutionChain: [120, 121],
  ),
  Pokemon(
    id: 121,
    name: "Staross",
    types: ["water", "psychic"],
    weaknesses: ["bug", "dark", "electric", "ghost", "grass"],
    description:
        "Staross nage en faisant tournoyer son corps en forme d'étoile,\nun peu à la manière d'une hélice de bateau. Le cœur au centre\ndu corps de ce Pokémon brille de sept couleurs.",
    height: "1,1 m",
    weight: "80,0 kg",
    category: "Mystérieux",
    abilities: "Médic Nature",
    color: Color(0xFF8f858c),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/121.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/121.png",
    evolutionChain: [120, 121],
  ),
  Pokemon(
    id: 122,
    name: "M. Mime",
    types: ["psychic", "fairy"],
    weaknesses: ["ghost", "steel", "poison"],
    description:
        "M. Mime est un pantomime hors pair. Ses gestes et ses\nmouvements parviennent à faire croire que quelque chose\nd'invisible existe réellement. Lorsqu'on y croit, ces choses\ndeviennent palpables.",
    height: "1,3 m",
    weight: "54,5 kg",
    category: "Bloqueur",
    abilities: "Anti-Bruit",
    color: Color(0xFFa493a0),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/122.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/122.png",
    evolutionChain: [122],
  ),
  Pokemon(
    id: 123,
    name: "Insécateur",
    types: ["bug", "flying"],
    weaknesses: ["rock", "electric", "fire", "flying", "ice"],
    description:
        "Insécateur est incroyablement rapide. Sa vitesse fulgurante\naméliore l'efficacité des deux lames situées sur ses avant-bras.\nElles sont si coupantes qu'elles peuvent trancher un énorme\ntronc d'arbre en un coup.",
    height: "1,5 m",
    weight: "56,0 kg",
    category: "Mante",
    abilities: "Essaim",
    color: Color(0xFF9cb397),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/123.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/123.png",
    evolutionChain: [123],
  ),
  Pokemon(
    id: 124,
    name: "Lippoutou",
    types: ["ice", "psychic"],
    weaknesses: ["bug", "dark", "fire", "ghost", "rock", "steel"],
    description:
        "Lippoutou marche en rythme, ondule de tout son corps et\nse déhanche comme s'il dansait. Ses mouvements sont si\ncommunicatifs que les gens qui le voient sont soudain pris\nd'une terrible envie de bouger les hanches, sans réfléchir.",
    height: "1,4 m",
    weight: "40,6 kg",
    category: "Humanoïde",
    abilities: "Benêt",
    color: Color(0xFFb78982),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/124.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/124.png",
    evolutionChain: [124],
  ),
  Pokemon(
    id: 125,
    name: "Élektek",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "Lorsqu'une tempête approche, des groupes entiers de\nce Pokémon se battent pour grimper sur les hauteurs,\noù la foudre a le plus de chance de tomber. Certaines villes\nse servent d'Élektek en guise de paratonnerres.",
    height: "1,1 m",
    weight: "30,0 kg",
    category: "Électrique",
    abilities: "Statik",
    color: Color(0xFF958862),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/125.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/125.png",
    evolutionChain: [125],
  ),
  Pokemon(
    id: 126,
    name: "Magmar",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "Lorsqu'il se bat, Magmar fait jaillir des flammes de son corps\npour intimider son adversaire. Les explosions enflammées de\nce Pokémon déclenchent des vagues de chaleur qui embrasent\nla végétation environnante.",
    height: "1,3 m",
    weight: "44,5 kg",
    category: "Crache-Feu",
    abilities: "Corps Ardent",
    color: Color(0xFFb88356),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/126.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/126.png",
    evolutionChain: [126],
  ),
  Pokemon(
    id: 127,
    name: "Scarabrute",
    types: ["bug"],
    weaknesses: ["fire", "flying", "rock"],
    description:
        "Scarabrute est doté de cornes imposantes. Des pointes\njaillissent de la surface de ses cornes. Ces pointes s'enfoncent\nprofondément dans le corps de l'ennemi, l'empêchant ainsi\nde s'échapper.",
    height: "1,7 m",
    weight: "59,0 kg",
    category: "Scarabée",
    abilities: "Peau Céleste",
    color: Color(0xFF8f8687),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/127.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/127.png",
    evolutionChain: [127],
  ),
  Pokemon(
    id: 128,
    name: "Tauros",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Ce Pokémon n'est pas satisfait s'il ne détruit pas tout sur\nson passage. Lorsque Tauros ne trouve pas d'adversaire,\nil se rue sur de gros arbres et les déracine pour passer\nses nerfs.",
    height: "1,4 m",
    weight: "88,4 kg",
    category: "Buffle",
    abilities: "Intimidation",
    color: Color(0xFF907863),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/128.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/128.png",
    evolutionChain: [128],
  ),
  Pokemon(
    id: 129,
    name: "Magicarpe",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Magicarpe est virtuellement inutile en combat. Il se contente\nde faire des ronds dans l'eau. On le considère plutôt faible.\nPourtant, ce Pokémon est très robuste et peut survivre\ndans n'importe quel environnement, même très pollué.",
    height: "0,9 m",
    weight: "10,0 kg",
    category: "Poisson",
    abilities: "Glissade",
    color: Color(0xFFc58e79),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/129.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/129.png",
    evolutionChain: [129, 130],
  ),
  Pokemon(
    id: 130,
    name: "Léviator",
    types: ["water", "flying"],
    weaknesses: ["electric", "rock"],
    description:
        "Lorsque Léviator commence à s'énerver, sa nature violente\nne se calme qu'une fois qu'il a tout réduit en cendres.\nLa fureur de ce Pokémon peut durer pendant un mois.",
    height: "6,5 m",
    weight: "305,0 kg",
    category: "Terrifiant",
    abilities: "Brise Moule",
    color: Color(0xFF708a94),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/130.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/130.png",
    evolutionChain: [129, 130],
  ),
  Pokemon(
    id: 131,
    name: "Lokhlass",
    types: ["water", "ice"],
    weaknesses: ["electric", "fighting", "grass", "rock"],
    description:
        "Les Lokhlass sont en voie d'extinction. Le soir, on entend\nce Pokémon chantonner une complainte mélancolique,\nespérant retrouver ses rares congénères.",
    height: "2,5 m",
    weight: "220,0 kg",
    category: "Transport",
    abilities: "Absorb Eau",
    color: Color(0xFF7aa1b6),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/131.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/131.png",
    evolutionChain: [131],
  ),
  Pokemon(
    id: 132,
    name: "Métamorph",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Métamorph peut modifier sa structure moléculaire pour\nprendre d'autres formes. Lorsqu'il essaie de se transformer\nde mémoire, il lui arrive de se tromper sur certains détails.",
    height: "0,3 m",
    weight: "4,0 kg",
    category: "Morphing",
    abilities: "Échauffement",
    color: Color(0xFFb3a2c3),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/132.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/132.png",
    evolutionChain: [132],
  ),
  Pokemon(
    id: 133,
    name: "Évoli",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Évoli a une structure génétique instable qui se transforme\nen fonction de l'environnement dans lequel il vit.\nCe Pokémon peut évoluer grâce aux radiations de\ndiverses pierres.",
    height: "0,3 m",
    weight: "6,5 kg",
    category: "Évolutif",
    abilities: "Fuite",
    color: Color(0xFF9f805e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/133.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/133.png",
    evolutionChain: [133, 134, 135, 136],
  ),
  Pokemon(
    id: 134,
    name: "Aquali",
    types: ["water"],
    weaknesses: ["electric", "grass"],
    description:
        "Aquali a subi une mutation spontanée. Des nageoires et\ndes branchies sont apparues sur son corps, ce qui lui permet\nde vivre dans les fonds marins. Ce Pokémon peut contrôler\nl'eau à volonté.",
    height: "1,0 m",
    weight: "29,0 kg",
    category: "Bulleur",
    abilities: "Absorb Eau",
    color: Color(0xFF7aa7b0),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/134.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/134.png",
    evolutionChain: [133, 134, 135, 136],
  ),
  Pokemon(
    id: 135,
    name: "Voltali",
    types: ["electric"],
    weaknesses: ["ground"],
    description:
        "Les cellules de Voltali génèrent un courant de faible intensité.\nCe pouvoir est amplifié par l'électricité statique de ses poils,\nce qui lui permet d'envoyer des éclairs. Sa fourrure hérissée\nest faite d'aiguilles chargées d'électricité.",
    height: "0,8 m",
    weight: "24,5 kg",
    category: "Orage",
    abilities: "Absorb Volt",
    color: Color(0xFFc6b076),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/135.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/135.png",
    evolutionChain: [133, 134, 135, 136],
  ),
  Pokemon(
    id: 136,
    name: "Pyroli",
    types: ["fire"],
    weaknesses: ["ground", "rock", "water"],
    description:
        "La fourrure soyeuse de Pyroli a une fonction anatomique.\nElle rejette la chaleur dans l'air pour que son corps ne\nsurchauffe pas. La température du corps de ce Pokémon\npeut atteindre 900 °C.",
    height: "0,9 m",
    weight: "25,0 kg",
    category: "Flamme",
    abilities: "Torche",
    color: Color(0xFFbd9066),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/136.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/136.png",
    evolutionChain: [133, 134, 135, 136],
  ),
  Pokemon(
    id: 137,
    name: "Porygon",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Porygon est capable de se décompiler et de retourner à l'état\nde programme informatique pour entrer dans le cyberespace.\nCe Pokémon est protégé contre le piratage, il est donc\nimpossible de le copier.",
    height: "0,8 m",
    weight: "36,5 kg",
    category: "Virtuel",
    abilities: "Calque",
    color: Color(0xFF8e8d94),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/137.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/137.png",
    evolutionChain: [137],
  ),
  Pokemon(
    id: 138,
    name: "Amonita",
    types: ["rock", "water"],
    weaknesses: ["grass", "electric", "fighting", "ground"],
    description:
        "Amonita est l'un des Pokémon disparus depuis longtemps\net qui furent ressuscités à partir de fossiles.\nLorsqu'il est attaqué par un ennemi, il se rétracte dans\nsa coquille.",
    height: "0,4 m",
    weight: "7,5 kg",
    category: "Spirale",
    abilities: "Coque Armure",
    color: Color(0xFFa3b1a4),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/138.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/138.png",
    evolutionChain: [138, 139],
  ),
  Pokemon(
    id: 139,
    name: "Amonistar",
    types: ["rock", "water"],
    weaknesses: ["grass", "electric", "fighting", "ground"],
    description:
        "Amonistar utilise ses tentacules pour capturer ses proies.\nOn pense que l'espèce s'est éteinte parce que sa coquille\nétait devenue trop grande et trop lourde, ce qui rendait\nses mouvements lents et pesants.",
    height: "1,0 m",
    weight: "35,0 kg",
    category: "Spirale",
    abilities: "Coque Armure",
    color: Color(0xFF8d9f99),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/139.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/139.png",
    evolutionChain: [138, 139],
  ),
  Pokemon(
    id: 140,
    name: "Kabuto",
    types: ["rock", "water"],
    weaknesses: ["grass", "electric", "fighting", "ground"],
    description:
        "Kabuto est un Pokémon ressuscité à partir d'un fossile.\nCependant, on a découvert des spécimens vivants.\nCe Pokémon n'a pas changé depuis 300 millions d'années.",
    height: "0,5 m",
    weight: "11,5 kg",
    category: "Carapace",
    abilities: "Glissade",
    color: Color(0xFF765d48),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/140.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/140.png",
    evolutionChain: [140, 141],
  ),
  Pokemon(
    id: 141,
    name: "Kabutops",
    types: ["rock", "water"],
    weaknesses: ["grass", "electric", "fighting", "ground"],
    description:
        "Jadis, Kabutops plongeait dans les profondeurs pour trouver\nses proies. Apparemment, ce Pokémon vivant sur terre est\nl'évolution d'une créature marine, comme le prouvent\nles changements dans ses branchies.",
    height: "1,3 m",
    weight: "40,5 kg",
    category: "Carapace",
    abilities: "Glissade",
    color: Color(0xFF8e7867),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/141.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/141.png",
    evolutionChain: [140, 141],
  ),
  Pokemon(
    id: 142,
    name: "Ptéra",
    types: ["rock", "flying"],
    weaknesses: ["electric", "ice", "rock", "steel", "water"],
    description:
        "Ptéra est un Pokémon de l'ère des dinosaures. Il fut ressuscité\nà partir de cellules extraites d'un morceau d'ambre. On pense\nqu'il était le roi des cieux à l'époque préhistorique.",
    height: "2,1 m",
    weight: "79,0 kg",
    category: "Fossile",
    abilities: "Griffe Dure",
    color: Color(0xFF958b9f),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/142.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/142.png",
    evolutionChain: [142],
  ),
  Pokemon(
    id: 143,
    name: "Ronflex",
    types: ["normal"],
    weaknesses: ["fighting"],
    description:
        "Les journées de Ronflex se résument aux repas et aux siestes.\nC'est un Pokémon tellement gentil que les enfants n'hésitent\npas à jouer sur son énorme ventre.",
    height: "2,1 m",
    weight: "460,0 kg",
    category: "Pionceur",
    abilities: "Isograisse",
    color: Color(0xFF9fabac),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/143.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/143.png",
    evolutionChain: [143],
  ),
  Pokemon(
    id: 144,
    name: "Artikodin",
    types: ["ice", "flying"],
    weaknesses: ["rock", "electric", "fire", "steel"],
    description:
        "Artikodin est un Pokémon Oiseau légendaire qui peut\ncontrôler la glace. Le battement de ses ailes gèle l'air\ntout autour de lui. C'est pourquoi on dit que lorsque\nce Pokémon vole, il va neiger.",
    height: "1,7 m",
    weight: "55,4 kg",
    category: "Glaciaire",
    abilities: "Pression",
    color: Color(0xFF6296b4),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/144.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/144.png",
    evolutionChain: [144],
  ),
  Pokemon(
    id: 145,
    name: "Électhor",
    types: ["electric", "flying"],
    weaknesses: ["ice", "rock"],
    description:
        "Électhor est un Pokémon Oiseau légendaire capable de\ncontrôler l'électricité. Il vit généralement dans les nuages\norageux. Ce Pokémon gagne en puissance lorsqu'il est\nfrappé par la foudre.",
    height: "1,6 m",
    weight: "52,6 kg",
    category: "Électrique",
    abilities: "Pression",
    color: Color(0xFFb9a068),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/145.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/145.png",
    evolutionChain: [145],
  ),
  Pokemon(
    id: 146,
    name: "Sulfura",
    types: ["fire", "flying"],
    weaknesses: ["rock", "electric", "water"],
    description:
        "Sulfura est un Pokémon Oiseau légendaire capable de\ncontrôler le feu. On raconte que lorsque ce Pokémon est\nblessé, il se baigne dans le magma en ébullition d'un volcan\npour se soigner.",
    height: "2,0 m",
    weight: "60,0 kg",
    category: "Flamme",
    abilities: "Pression",
    color: Color(0xFFdea16e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/146.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/146.png",
    evolutionChain: [146],
  ),
  Pokemon(
    id: 147,
    name: "Minidraco",
    types: ["dragon"],
    weaknesses: ["dragon", "ice", "fairy"],
    description:
        "Minidraco mue constamment, renouvelant sans arrêt sa peau.\nEn effet, l'énergie vitale de son corps augmente régulièrement\net sa mue lui permet d'éviter d'atteindre des niveaux\nincontrôlables.",
    height: "1,8 m",
    weight: "3,3 kg",
    category: "Dragon",
    abilities: "Mue",
    color: Color(0xFFadb4c3),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/147.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/147.png",
    evolutionChain: [147, 148, 149],
  ),
  Pokemon(
    id: 148,
    name: "Draco",
    types: ["dragon"],
    weaknesses: ["dragon", "ice", "fairy"],
    description:
        "Draco stocke une quantité d'énergie considérable dans\nson corps. On raconte qu'il peut modifier les conditions\nclimatiques autour de lui en déchargeant l'énergie contenue\ndans les cristaux de son cou et de sa queue.",
    height: "4,0 m",
    weight: "16,5 kg",
    category: "Dragon",
    abilities: "Mue",
    color: Color(0xFF9cb4c9),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/148.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/148.png",
    evolutionChain: [147, 148, 149],
  ),
  Pokemon(
    id: 149,
    name: "Dracolosse",
    types: ["dragon", "flying"],
    weaknesses: ["ice", "dragon", "rock", "fairy"],
    description:
        "Dracolosse est capable de faire le tour de la planète en seize\nheures à peine. C'est un Pokémon au grand cœur qui ramène\nà bon port les navires perdus dans les tempêtes.",
    height: "2,2 m",
    weight: "210,0 kg",
    category: "Dragon",
    abilities: "Attention",
    color: Color(0xFFc0a97e),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/149.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/149.png",
    evolutionChain: [147, 148, 149],
  ),
  Pokemon(
    id: 150,
    name: "Mewtwo",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "Mewtwo est un Pokémon créé par manipulation génétique.\nCependant, bien que les connaissances scientifiques des\nhumains aient réussi à créer son corps, elles n'ont pas pu\ndoter Mewtwo d'un cœur sensible.",
    height: "1,5 m",
    weight: "33,0 kg",
    category: "Génétique",
    abilities: "Insomnia",
    color: Color(0xFFafa2b1),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/150.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/150.png",
    evolutionChain: [150],
  ),
  Pokemon(
    id: 151,
    name: "Mew",
    types: ["psychic"],
    weaknesses: ["bug", "dark", "ghost"],
    description:
        "On dit que Mew possède le code génétique de tous les autres\nPokémon. Il peut se rendre invisible à sa guise, ce qui lui\npermet de ne pas se faire remarquer quand il s'approche\ndes gens.",
    height: "0,4 m",
    weight: "4,0 kg",
    category: "Nouveau",
    abilities: "Synchro",
    color: Color(0xFFc9a7b1),
    smallImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/151.png",
    largeImageUrl:
        "https://assets.pokemon.com/assets/cms2/img/pokedex/full/151.png",
    evolutionChain: [151],
  )
];
