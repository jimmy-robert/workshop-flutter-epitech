import 'package:epitech_pokemons/pokemon_list.dart';
import 'package:flutter/material.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pokémon for Epitech',
      theme: ThemeData(fontFamily: "Pokemon"),
      home: PokemonList(),
    );
  }
}
