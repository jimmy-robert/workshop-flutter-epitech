import 'package:epitech_pokemons/pokemon.dart';
import 'package:epitech_pokemons/pokemon_tile.dart';
import 'package:epitech_pokemons/pokemons_by_id.dart';
import 'package:epitech_pokemons/type_column.dart';
import 'package:flutter/material.dart';
import 'package:tinycolor/tinycolor.dart';

class PokemonDetail extends StatelessWidget {
  final Pokemon pokemon;

  const PokemonDetail({this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TinyColor(pokemon.color).saturate(20).color,
      body: ListView(
        padding: EdgeInsets.all(16),
        children: <Widget>[
          SafeArea(child: Container()),
          Hero(
            tag: pokemon.id,
            child: PokemonTile(pokemon: pokemon),
          ),
          SizedBox(height: 8),
          HorizontalList(
            children:
                pokemon.types.map((type) => TypeColumn(type: type)).toList(),
          ),
          SizedBox(height: 28),
          Text(
            "Description",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black.withOpacity(0.6),
              fontWeight: FontWeight.w600,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 8),
          Container(
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.9),
              borderRadius: BorderRadius.circular(12),
            ),
            padding: EdgeInsets.all(16),
            child: Text(
              pokemon.description.replaceAll("\n", " "),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black.withOpacity(0.75)),
            ),
          ),
          SizedBox(height: 28),
          Text(
            "Infos",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black.withOpacity(0.6),
              fontWeight: FontWeight.w600,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 8),
          HorizontalList(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    "Height",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    pokemon.height,
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    "Weigh",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    pokemon.weight,
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Text(
                    "Talent",
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    pokemon.abilities,
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.6),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(height: 28),
          Text(
            "Faiblesses",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black.withOpacity(0.6),
              fontWeight: FontWeight.w600,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 8),
          HorizontalList(
            children: pokemon.weaknesses
                .map((type) => TypeColumn(type: type))
                .toList(),
          ),
          SizedBox(height: 28),
          Text(
            "Chaîne d'évolution",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black.withOpacity(0.6),
              fontWeight: FontWeight.w600,
              fontSize: 20,
            ),
          ),
          SizedBox(height: 8),
          ...pokemon.evolutionChain
              .map<Widget>(
                (id) => PokemonTile(pokemon: pokemonsById[id]),
              )
              .separatedWith(
                separator: SizedBox(height: 8),
              ),
        ],
      ),
    );
  }
}

class HorizontalList extends StatelessWidget {
  final List<Widget> children;

  HorizontalList({this.children});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.9),
        borderRadius: BorderRadius.circular(12),
      ),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      child: Row(
        children: children
            .map<Widget>(
              (child) => Expanded(child: child),
            )
            .separatedWith(
              separator: Container(
                margin: EdgeInsets.symmetric(horizontal: 8),
                width: 1,
                height: 48,
                color: Colors.black.withOpacity(0.05),
              ),
            )
            .toList(),
      ),
    );
  }
}

extension<T> on Iterable<T> {
  Iterable<T> separatedWith({T separator}) {
    final result = List<T>.from(this);
    for (int index = length - 1; index > 0; index--) {
      result.insert(index, separator);
    }
    return result;
  }
}
