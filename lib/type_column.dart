import 'package:flutter/material.dart';

class TypeColumn extends StatelessWidget {
  final String type;

  const TypeColumn({this.type});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: SizedBox(
          width: 60,
          child: Column(
            children: <Widget>[
              Image.asset(
                "assets/types/$type.png",
                height: 36,
                width: 36,
              ),
              SizedBox(height: 4),
              Text(
                _getReadableType(type),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black.withOpacity(0.6),
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
                textScaleFactor: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _getReadableType(String type) {
    switch (type) {
      case "grass":
        return "Plante";
      case "poison":
        return "Poison";
      case "fire":
        return "Feu";
      case "flying":
        return "Vol";
      case "bug":
        return "Insecte";
      case "water":
        return "Eau";
      case "normal":
        return "Normal";
      case "electric":
        return "Électrik";
      case "ground":
        return "Sol";
      case "fighting":
        return "Combat";
      case "fairy":
        return "Fée";
      case "psychic":
        return "Psy";
      case "rock":
        return "Roche";
      case "steel":
        return "Acier";
      case "ice":
        return "Glace";
      case "dragon":
        return "Dragon";
      case "ghost":
        return "Spectre";
      case "dark":
        return "Ténèbr.";
      default:
        return "-";
    }
  }
}
