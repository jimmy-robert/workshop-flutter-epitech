import 'package:epitech_pokemons/pokemon.dart';
import 'package:epitech_pokemons/pokemon_detail.dart';
import 'package:epitech_pokemons/type_ball.dart';
import 'package:flutter/material.dart';
import 'package:tinycolor/tinycolor.dart';

class PokemonTile extends StatelessWidget {
  final Pokemon pokemon;

  PokemonTile({this.pokemon});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      elevation: 0,
      margin: EdgeInsets.all(0),
      color: TinyColor(pokemon.color).saturate(25).color,
      clipBehavior: Clip.antiAlias,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.2),
                border: Border.all(
                  color: Colors.black12,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(16),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            "#${pokemon.id.toString().padLeft(3, "0")}",
                            style: TextStyle(
                              color: Colors.black.withOpacity(0.75),
                              fontSize: 16,
                            ),
                          ),
                          SizedBox(width: 12),
                          Expanded(
                            child: Text(
                              pokemon.name,
                              style: TextStyle(
                                color: Colors.black.withOpacity(0.7),
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 6),
                      Text(
                        "Pokémon ${pokemon.category}",
                        style: TextStyle(
                          color: Colors.black.withOpacity(0.6),
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 16),
                Stack(
                  children: <Widget>[
                    if (pokemon.types.length == 1)
                      TypeBall(type: pokemon.types[0]),
                    if (pokemon.types.length > 1)
                      Transform.translate(
                        offset: Offset(-14, -12),
                        child: TypeBall(type: pokemon.types[0]),
                      ),
                    if (pokemon.types.length > 1)
                      Transform.translate(
                        offset: Offset(14, 12),
                        child: TypeBall(type: pokemon.types[1]),
                      ),
                  ],
                ),
                SizedBox(width: 12),
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      transform: Matrix4.translationValues(4, 0, 0),
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.4),
                        shape: BoxShape.circle,
                      ),
                    ),
                    Image.network(
                      pokemon.smallImageUrl,
                      height: 80,
                      width: 80,
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned.fill(
            child: InkWell(
              highlightColor: Colors.black12,
              splashColor: Colors.black12,
              onTap: () {
                final route = MaterialPageRoute(
                  builder: (BuildContext context) {
                    return PokemonDetail(pokemon: pokemon);
                  },
                );
                Navigator.of(context).push(route);
              },
            ),
          ),
        ],
      ),
    );
  }
}
