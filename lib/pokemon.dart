import 'package:flutter/material.dart';

class Pokemon {
  final int id;
  final String name;
  final List<String> types;
  final List<String> weaknesses;
  final String description;
  final String height;
  final String weight;
  final String category;
  final String abilities;
  final Color color;
  final String smallImageUrl;
  final String largeImageUrl;
  final List<int> evolutionChain;

  const Pokemon({
    this.id,
    this.name,
    this.types,
    this.weaknesses,
    this.description,
    this.height,
    this.weight,
    this.category,
    this.abilities,
    this.color,
    this.smallImageUrl,
    this.largeImageUrl,
    this.evolutionChain,
  });

  @override
  String toString() {
    final number = id.toString().padLeft(3, '0');
    return "$number - $name";
  }
}
