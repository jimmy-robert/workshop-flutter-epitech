import 'package:flutter/material.dart';

class TypeBall extends StatelessWidget {
  final String type;

  const TypeBall({this.type});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.75),
          shape: BoxShape.circle,
          border: Border.all(
            color: Colors.white,
            width: 2,
          )),
      child: Image.asset(
        "assets/types/$type.png",
        height: 24,
        width: 24,
      ),
    );
  }
}
