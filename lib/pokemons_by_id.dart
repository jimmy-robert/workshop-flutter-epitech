import 'package:epitech_pokemons/pokemon.dart';
import 'package:epitech_pokemons/pokemons.dart';

final pokemonsById = Map<int, Pokemon>.fromIterable(
  pokemons,
  key: (pokemon) => pokemon.id,
  value: (pokemon) => pokemon,
);
