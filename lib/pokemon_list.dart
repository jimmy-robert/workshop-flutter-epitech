import 'package:epitech_pokemons/pokemon_tile.dart';
import 'package:epitech_pokemons/pokemons.dart';
import 'package:flutter/material.dart';

class PokemonList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFDFDFD),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Pokémon",
          style: TextStyle(
            color: Color(0xFF424242),
            fontWeight: FontWeight.bold,
          ),
        ),
        elevation: 2,
      ),
      body: ListView.separated(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
        itemCount: pokemons.length,
        itemBuilder: (context, index) {
          final pokemon = pokemons[index];
          return Hero(
            tag: pokemon.id,
            child: PokemonTile(pokemon: pokemon),
          );
        },
        separatorBuilder: (context, index) {
          return SizedBox(height: 8);
        },
      ),
    );
  }
}
